# Preparar o seu ambiente

Antes de qualquer coisa, verifique se a versão do seu pip é `9.0.1` ou maior.

Instale as ferramentas:

    sudo python3 -m pip install grpcio
    sudo python3 -m pip install grpcio-tools

(Mais info aqui: [docs](https://grpc.io/docs/quickstart/python.html))

Baixe e instale as dependêcias do projeto:

    python3 --module pip install -r requirements.txt

# Para gerar os arquivos `.py` a partir dos `protos`

Execute o comando:

    python3 -m grpc_tools.protoc -I proto --python_out=generated --grpc_python_out=generated proto/*.proto

# Executando o projeto

    PYTHONPATH=./generated python3 main.py

`PYTHONPATH=./generated` para tratar os arquivos dentro da pasta `generated` como se estivessem no diretŕoio principal.

# Créditos

Livro **Pride and Prejudice** obtido do Project Gutenberg [LINK](http://www.gutenberg.org/ebooks/1342) (O livro está no dominio público)
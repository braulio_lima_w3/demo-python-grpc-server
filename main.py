from server import GrpcConfigurer
from dotenv import load_dotenv
from services.demo_service import registerDemoService

load_dotenv()

instance = GrpcConfigurer([ registerDemoService ])

# run server
instance.run()
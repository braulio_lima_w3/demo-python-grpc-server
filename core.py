from random import randint

def fibonacci(times, recursion):
    print('Calling function fibonacci', times, recursion)
    def recusrive(times):
        if times == 0:
            return 0
        if times == 1:
            return 1
        return recusrive(times - 2) + recusrive(times - 1)
    def linear(times):
        current = 0
        previous = 0
        for i in range(times):
            if i == 0:
                current = 1
                continue
            temp = current
            current = current + previous
            previous = temp
        return current
    if type(times) != int or type(recursion) != bool:
        return 0
    if recursion:
        return recusrive(times)
    return linear(times)

prideAndPrejudice = []

text = open('pride-prej.txt', 'r')
for line in text:
    prep = line.strip()
    if not prep:
        continue
    prideAndPrejudice.append(prep)
text.close()

def random():
    print('Picking out a dandom line from Pride and Prejudice')
    return prideAndPrejudice[randint(0, len(prideAndPrejudice))]
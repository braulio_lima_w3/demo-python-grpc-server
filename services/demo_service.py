
import generated.demo_pb2 as demo_pb2
import generated.demo_pb2_grpc as demo_pb2_grpc
import core

class DemoService(demo_pb2_grpc.DemoServiceServicer):
    def calculateFibonacci(self, request, context):
        answer = core.fibonacci(request.iterations, request.recusrive)
        return demo_pb2.GrpcFibonacciResponse(
            answer = answer
        )

    def random(self, request, context):
        answer = core.random()
        return demo_pb2.StringAnswer(
            answer = answer
        )

def registerDemoService(server):
    if not server:
        return
    demo_pb2_grpc.add_DemoServiceServicer_to_server(DemoService(), server)